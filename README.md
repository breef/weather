[![CircleCI](https://circleci.com/gh/weupz/weather/tree/master.svg?style=svg)](https://circleci.com/gh/weupz/weather/tree/master)

### WeUp Z Weather

See the weather of your selected city. This is what this app doing, no ads, no nonsense.

Fastlane, CircleCI, Kotlin, MVI Architecture, Dagger, RxJava, Retrofit, Room, OpenWeather API
